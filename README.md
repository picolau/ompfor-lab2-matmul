# OpenMP -for- Labs

Let's learn parallel programming using OpenMP!

## Matmul

Matrix multiplication or matrix product is a binary operation that produces a matrix from two matrices. The matrix product is designed for representing the composition of linear maps that are represented by matrices.

See [Wikipedia](https://en.wikipedia.org/wiki/Matrix_multiplication).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor.
4. Parallelize the code using OpenMP.
5. Compare performance between serial and parallel version. Determine the speedup.
6. Experiment with different matrix sizes. 

Anything missing? Ideas for improvements? Make a pull request.
